## Dockerfiles for Haskell WebAssembly Backend (`wasm32-wasi`)

This is a Dockerfile for the [Haskell WebAssembly Backend](https://ghc.gitlab.haskell.org/ghc/doc/users_guide/wasm.html). It uses a Ubuntu image, installing the WebAssembly backend using the setup script in [`ghc-wasm-meta`](https://gitlab.haskell.org/ghc/ghc-wasm-meta).

The Dockerfile is split into two stages to significantly reduce the size of the resulting image. This allows it to be used in container registries, such as GitLab's, for use in CI.

### Build

```sh
$ docker build -t wasm32-wasi .
```

### Run

`ghc`, `ghc-pkg` and `hsc2hs` are available as `wasm32-wasi-ghc`, `wasm32-wasi-ghc-pkg` and `wasm32-wasi-hsc2hs`, respectively.

```sh
$ docker run --rm wasm32-wasi wasm32-wasi-ghc --version
The Glorious Glasgow Haskell Compilation System, version 9.8.0.20230927

$ docker run --rm wasm32-wasi wasm32-wasi-cabal --version
cabal-install version 3.10.1.0
compiled using version 3.10.1.0 of the Cabal library
```

To compile the cabal project in the current folder to a wasm module, mount it as the working directory:

```sh
$ docker run --rm -v "$(pwd)":/work -w /work wasm32-wasi wasm32-wasi-cabal build
```

